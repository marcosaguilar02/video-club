# Project Title

Práctica | Mi primera API con express + ORM

## Getting Started/ Installing

1. Clonar el repositorio

  `git clone -b ORM git@gitlab.com:marcosaguilar02/video-club.git`

3. Abrir una terminal en la raíz del repositorio

4. Ejecutar `$ npm install`

5. Asegurarse de tener un contenedor de `mysql` ejecutandose
   
5. Ejecutar comando(s):
   
  `$ npm run dev` | `$ npm start`

6. Abrir navegador y en la barra de navegación poner el url:

  `http://localhost:3000`

### Prerequisites

* [nodejs](https://nodejs.org/es)

* [npm](https://www.npmjs.com/)

* [expressjs](https://expressjs.com/)

* [Docker](https://www.docker.com/)

### And coding style tests

Estilo de escritura camelCase recomendado para java script.

## Built With

* Nodejs  
* JavaScript 
* Expressjs
* Sequelize 

## Contributing

No contribuyentes.

## Versioning

v0.0.0

## Author

Marcos Alfredo Aguilar Mata.

## License

ISC.